#!/usr/bin/env python
import sys

def fbw(x):
    results = []
    if x % 3 == 0:
        results.append("Fizz")
    for i in range(str(x).count("3")):
        results.append("Fizz")
    if x % 5 == 0:
        results.append("Buzz")
    for i in range(str(x).count("5")):
        results.append("Buzz")
    if x % 7 == 0:
        results.append("Wolf")
    for i in range(str(x).count("7")):
        results.append("Wolf")


    return " ".join(results)

#for line in sys.stdin:
for line in ["27 1\n"]:
    start,end = (line.strip().split())
    start,end = min(int(start), int (end)),max(int(start),int(end))
 #   print(start,end)
    numbers = range(start,end+1)
    for x in numbers:
        if fbw(x):
            print x, fbw(x)
        else:
            print x

