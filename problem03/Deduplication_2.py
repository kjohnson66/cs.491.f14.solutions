import sys
from collections import Counter
cnt = Counter()
for line in sys.stdin:
	cnt[line.strip()] += 1
y = sorted(filter(lambda x:cnt[x] > 1, cnt.keys()))
for key in y :
	print '{} ({})'.format(key, cnt[key])
