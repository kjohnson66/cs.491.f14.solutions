
#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      User
#
# Created:     27/09/2014
# Copyright:   (c) User 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from __future__ import print_function
from __future__ import division
import sys , re

def main():
    input = [
'INSERT 0\n',
'INSERT 1\n',
'INSERT 2\n',
'INSERT 3\n',
'INSERT 4\n',
'INSERT 5\n',
'INSERT 6\n',
'INSERT 7\n',
'INSERT 8\n',
'INSERT 9\n',
'SELECT X == 5\n',
'SELECT X != 5\n',
'SELECT X < 5\n',
'SELECT X <= 5\n',
'SELECT X > 5\n',
'SELECT X >= 5\n',
'COUNT X < 4\n',
'DELETE X < 4\n',
'COUNT X < 4\n'
]
    stripped = []
    scope = []
    for line in sys.stdin:
        stripped.append(line.strip())

    for line in stripped:
        result = re.search("INSERT\s+(\d+)", line, re.IGNORECASE)
        if result is not None:
            scope.append(int(result.group(1)))
            print (scope)
        result = re.search("SELECT\s+X\s*(!=|>=|<=|==|>|<)\s+(\d+)", line, re.IGNORECASE)
        if result is not None:
            selectscope = []
            for item in scope:
                if result.group(1) == "==" and item == int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == "!=" and item != int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == ">=" and item >= int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == "<=" and item <= int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == ">" and item > int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == "<" and item < int(result.group(2)):
                    selectscope.append(item)
            print (selectscope)
        result = re.search("DELETE\s+X\s*(!=|>=|<=|==|>|<)\s+(\d+)", line, re.IGNORECASE)
        if result is not None:
            selectscope = []
            for item in scope:
                if result.group(1) == "==" and item == int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == "!=" and item != int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == ">=" and item >= int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == "<=" and item <= int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == ">" and item > int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == "<" and item < int(result.group(2)):
                    selectscope.append(item)
            scope = [x for x in scope if x not in selectscope]
            print (scope)
        result = re.search("COUNT\s+X\s*(!=|>=|<=|==|>|<)\s+(\d+)", line,re.IGNORECASE)
        if result is not None:
            selectscope = []
            for item in scope:
                if result.group(1) == "==" and item == int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == "!=" and item != int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == ">=" and item >= int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == "<=" and item <= int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == ">" and item > int(result.group(2)):
                    selectscope.append(item)
                elif result.group(1) == "<" and item < int(result.group(2)):
                    selectscope.append(item)

            print (len(selectscope))

if __name__ == '__main__':
    main()