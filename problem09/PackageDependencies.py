#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      johnskie
#
# Created:     19/10/2014
# Copyright:   (c) johnskie 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import collections
import sys

class Cerror(Exception):
    pass

class Graph(object):

    def __init__(self, numvertices, stream) :
        self.vertices = collections.defaultdict(list)
        self.degrees  = collections.defaultdict(int)
        #initializes vertices and degrees to dicts of lists and ints respectively
        for i in range(numvertices):
            vertices = stream.readline().strip().split()
            vertex   = vertices[0]
            edges    = vertices[1:]

            self.vertices[vertex].extend(sorted(edges))
            # from our given i until the end of the range determined by the number of vertices, we assign
            #vertices to the result of our input after strip and split operations, then vertex and edge to
            #vertices at indices 0 and 1
            self.degrees[vertex]
            for edge in edges:
                self.degrees[edge] += 1

    def sorted(self):
        ordered  = []
        vertices = sorted([v for v, e in self.degrees.items() if e == 0])
#create new empty list to contain sorted vertices, then sort them based on v and e(vertex and edge)
        while vertices:
            top = vertices.pop(0)
            ordered.append(top)

            for v in self.vertices[top]:
                self.degrees[v] -= 1
                if self.degrees[v] == 0:
                    vertices.append(v)

            vertices.sort()

        if len(ordered) != len(self.vertices):
            raise Cerror

        return ordered

    def __str__(self):
        result = []
        for vertex, edges in sorted(self.vertices.items()):
            result.append(' '.join([str(self.degrees[vertex]), vertex] + edges))
        return '\n'.join(result)

while True:
    numvertices = int(sys.stdin.readline())
    if numvertices == 0:
        break

    graph = Graph(numvertices, sys.stdin)
    try:
        print " ".join(graph.sorted())
    except Cerror:
        print 'Cycle Detected'
