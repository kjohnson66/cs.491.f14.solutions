#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      johnskie
#
# Created:     23/11/2014
# Copyright:   (c) johnskie 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import sys
import operator

def calculate (scores1, scores2):
    index = operator.itemgetter(0)
    score = operator.itemgetter(1)
    #enumerate to create a list of (inex, score) tuples
    one = list(enumerate(scores1))
    two = list(enumerate(scores2))

#sort the tuples on item one using operator.itemgetter
    one.sort(key = score, reverse = True)
    two.sort(key = score, reverse = True)
    #check indices order
    for n, (a,b) in enumerate(zip(one, two)):
        if index(a) != index(b):
            #if not return rank
            print "Case",  str(case) + ":", n+1
            return
    #if yes
    print "Case", str(case) + ":" , "agree"



case = 1;
state = 0;
test = """4
3
 8 6 2
15 37 17 3
8
80 60 40 20 10
30 50 70
160 100 120
 80 20 60 90
   135""".split("\n")
# 0 indicates number of competitors
# 1 indicates judge 1
# 2 indicates judge 2
for line in sys.stdin:
#for line in test:
    line = line.strip().split()
    if not line:
        continue
   # print ("State", state, line) if newline, " " , newline then strips out space and has empty list
    if state == 0:
        #if empty line, error
        competitors = int(line[0])
        state = 1;
        scores1 = [];
        scores2 = [];
    else:
        for y in line:
            if state == 1:
                scores1.append(int(y))
                if len(scores1) >= competitors:
                    state = 2;
            elif state == 2:
                scores2.append(int(y))
                if len(scores2) >= competitors:
                    state = 0;
                    calculate(scores1, scores2);
                    case += 1;
