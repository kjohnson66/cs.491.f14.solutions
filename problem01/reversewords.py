#! usr/bin/env python
#create the list, import the required protocols

import sys
import string
# accept input from sys.stdin and add the lines to the list
for line in sys.stdin :
#for line in ["Microsoft's evils.\n",  "Open Source is FTW.\n",  "Fear not, the reaper.", "This is, a Test String,", "Testing, if the first word is a comma"]:
    rev_line = list(reversed(line.split()))
    end_punct = rev_line[0][-1]
    rev_line[0] = rev_line[0][0:-1]
    #end_punct holds the sliced period, rev_line set to a slice of itself minus the last character of the first reversed word(i.e the period)
    for index, word in enumerate(rev_line):
        if word[-1] == ",":
            rev_line[index] = word[0:-1]
            rev_line[index-1] += ","
    rev_line[-1] += end_punct





    print(" ".join(rev_line))
