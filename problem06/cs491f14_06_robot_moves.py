#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      johnskie
#
# Created:     04/10/2014
# Copyright:   (c) johnskie 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function
from __future__ import division
import sys

def cell_in_threshold(threshold, row, column):
    digits = str(row) + str(column)
    nums = [int(digit) for digit in digits]
    if threshold >=  sum(nums):
        return True
    return False
def main():
    input = []
    #for line in ["2 3 3\n", "5 5 5\n", "-1 -1 -1\n", "5 5 5\n"]:
    for line in sys.stdin:
        #print(line.strip().split())
        if "-1" in line:
            break
        input.append([int(x) for x in line.strip().split()])
    #print (input)
    for threshold, row, column in input:
        count = 0
        for r in range(row):
            for c in range(column):
                if cell_in_threshold(threshold,r,c):
                    count += 1
        print(count)
       # print (threshold, row, column)
if __name__ == '__main__':
    main()
